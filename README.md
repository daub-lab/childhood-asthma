This repository contains 38 ctss files and the asthma expression table from Persson et al. (https://doi.org/10.1016/j.jaci.2015.02.026)

To download the files use the following command:

`git clone git@gitlab.com:daub-lab/childhood-asthma.git`
